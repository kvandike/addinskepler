﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Threading.Tasks;

namespace Addins_Kepler
    {
    public enum ExportFormat
        {
        FBX
        }

    class MainExport
        {
        private ExternalCommandData e_commandData;

        // 3d ли это?
        private bool e_isI3D;

        public MainExport(ExternalCommandData commandData)
            {
            e_commandData = commandData;
            if(commandData.Application.ActiveUIDocument.Document.ActiveView.ViewType == Autodesk.Revit.DB.ViewType.ThreeD)
                {
                e_isI3D = true;
                }
            else
                {
                e_isI3D = false;
                }
            Export();
            }
        public void Export()
            {
            ExportFormat format = ExportFormat.FBX;
            switch(format)
                {
                case ExportFormat.FBX:
                    ExportFBX exportFbx = new ExportFBX(e_commandData, ExportData.ExportFormat.FBX);
                    break;
                }
            }

        }
    }
