﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace Addins_Kepler
{
    class ExportData
    {

        public enum ExportFormat
            {
            FBX
            }


        //активная директория сохранения
        protected String e_Folder;
        //имя файла
        protected String e_NameExport;
        //активный документ
        protected Document e_ActiveDocument;
        //параметры активного документа
        protected String e_NameActive;
        protected String e_NameView;
        //формат экспорта
        protected ExportFormat e_ExportFormat;
        //имя экспортируемого файла
        protected String e_NameExFormt;
        //фильтр экспорта
        protected String e_filterExport;
        //помощь к документу нид
        protected  ExternalCommandData e_commandData;
        //чтож, надо узнать, это 3d или нет)
        protected bool isI3D;

        //конструктор
        public ExportData(ExternalCommandData commandData, ExportFormat exportFormat)
            {
            e_commandData = commandData;
            e_ActiveDocument = commandData.Application.ActiveUIDocument.Document;
            e_ExportFormat = exportFormat;
            e_filterExport = String.Empty;
            Initialize();
            }

        private void Initialize()
            {
            //пока влепил эту конечную папку
            string md = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            if(System.IO.Directory.Exists(md+"\\ProjectKepler\\ExportedModels")==false)
                {
                string ee= md+"\\ProjectKepler";
                System.IO.Directory.CreateDirectory(ee);
                ee =md +"\\ProjectKepler\\ExportedModels";
                System.IO.Directory.CreateDirectory(ee);
                }
                e_Folder =md + @"\ProjectKepler\ExportedModels\";
            //собираем потихоньку данные об файле
            e_NameActive = e_ActiveDocument.Title;
            e_NameView = e_ActiveDocument.ActiveView.Name;
            String viewType = e_ActiveDocument.ActiveView.ViewType.ToString();
            //имя экспортируемого файла
            e_NameExport = e_NameActive + "-" + viewType + "-" + e_NameView + "." + EndFormat().ToString();
            //смотрим, это 3d или нет
            if(e_ActiveDocument.ActiveView.ViewType == Autodesk.Revit.DB.ViewType.ThreeD)
                {
                isI3D = true;
                }
            else
                {
                isI3D = false;
                }
            }




        //в какой формат
        private String EndFormat()
            {
            String temp = null;
            switch(e_ExportFormat)
                {
                case ExportFormat.FBX:
                    temp = "fbx";
                    break;
                }
            return temp;
            }
    }
}
