﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace Addins_Kepler
{
    class ExportFBX : ExportData
    {
         public ExportFBX(ExternalCommandData commandData, ExportFormat exportFormat)
            : base(commandData, exportFormat)
        {
            e_filterExport = "FBX Files |*.fbx";
            bool exported = false;
            //параметры
            ViewSet views = new ViewSet();
            views.Insert(e_ActiveDocument.ActiveView);
            FBXExportOptions options = new FBXExportOptions();
            options.UseLevelsOfDetail = true;
            options.LevelsOfDetailValue = 0;
             //не запустим приложение, пока не создастся файл
            exported = e_ActiveDocument.Export(e_Folder, e_NameExport, views, options);
            do
                {
                System.Threading.Thread.Sleep(1000);
                }
            while((System.IO.File.Exists(e_Folder + e_NameExport) ==false));
            string pf=Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)+@"\ProjectKepler\ProjectKepler.exe";
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName =pf;
            p.StartInfo.Arguments ="-importmodel "+ e_Folder+e_NameExport;
            p.Start();
        }
    }
}
