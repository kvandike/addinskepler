﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Reflection;
using System.IO;

namespace Addins_Kepler
    {
        [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Automatic)]
        [Autodesk.Revit.Attributes.Regeneration(Autodesk.Revit.Attributes.RegenerationOption.Manual)]
        [Autodesk.Revit.Attributes.Journaling(Autodesk.Revit.Attributes.JournalingMode.NoCommandData)]
        public class goKepler : IExternalCommand
            {
            public Autodesk.Revit.UI.Result Execute(ExternalCommandData commandData,
            ref string message, Autodesk.Revit.DB.ElementSet elements)
                {
                try
                    {
                    if(null == commandData.Application.ActiveUIDocument.Document)
                        {
                        message = "Active view is null.";
                        return Autodesk.Revit.UI.Result.Failed;
                        }

                    MainExport mainData = new MainExport(commandData);
                    // Show the dialog
                    }
                catch(Exception ex)
                    {
                    message = ex.ToString();
                    return Autodesk.Revit.UI.Result.Failed;
                    }
                return Autodesk.Revit.UI.Result.Succeeded;
                }
            }
    }
