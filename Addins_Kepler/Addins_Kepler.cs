﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Reflection;
using System.Windows.Media.Imaging;

namespace Kepler
    {
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Automatic)]
    [Autodesk.Revit.Attributes.Regeneration(Autodesk.Revit.Attributes.RegenerationOption.Manual)]
    [Autodesk.Revit.Attributes.Journaling(Autodesk.Revit.Attributes.JournalingMode.NoCommandData)]
    public class Addins_Kepler : Autodesk.Revit.UI.IExternalApplication
        {
        static string AddInPath = typeof(Addins_Kepler).Assembly.Location;
        static string tabName = "Project Kepler";

        public Autodesk.Revit.UI.Result OnStartup(UIControlledApplication application)
            {
            application.CreateRibbonTab(tabName);
            Uri uriImage = new Uri(@"C:\Program Files\ProjectKepler\RevitPlugins\ico_kepler.png");
            RibbonPanel ribbonPanel = application.CreateRibbonPanel(tabName, "Project Kepler");
            PushButton pushButton = ribbonPanel.AddItem(new PushButtonData("Kepler Export",
            "Kepler Export", AddInPath, "Addins_Kepler.goKepler")) as PushButton;
            BitmapImage largeImage = new BitmapImage(uriImage);
            pushButton.LargeImage = largeImage;
            return Result.Succeeded;
            }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="application"></param>
        /// <returns></returns>
        public Result OnShutdown(UIControlledApplication application)
            {
            return Result.Succeeded;
            }
        }
    }

/*namespace Addins_Kepler
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    [Autodesk.Revit.Attributes.Regeneration(Autodesk.Revit.Attributes.RegenerationOption.Manual)]
    [Autodesk.Revit.Attributes.Journaling(Autodesk.Revit.Attributes.JournalingMode.NoCommandData)]
    public class Addins_Kepler : IExternalCommand
        {
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData commandData,
        ref string message, Autodesk.Revit.DB.ElementSet elements)
            {
            try
                {
                if(null == commandData.Application.ActiveUIDocument.Document)
                    {
                    message = "Active view is null.";
                    return Autodesk.Revit.UI.Result.Failed;
                    }

                MainExport mainData = new MainExport(commandData);
                // Show the dialog
                }
            catch(Exception ex)
                {
                message = ex.ToString();
                return Autodesk.Revit.UI.Result.Failed;
                }
            return Autodesk.Revit.UI.Result.Succeeded;
            }
        }
}
*/